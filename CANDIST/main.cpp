#include <iostream>
using namespace std;

int main() 
{
    int people = 0;
    cin >> people;
    int types_n = 0;
    cin >> types_n;

    // amount of each type is divided among all people, rounding down
    int types[types_n];
    for(int i = 0; i < types_n; i++)
    {
        cin >> types[i];
        types[i] = (int)types[i]/people;
    }

    // display results
    for(int i = 0; i < types_n; i++)
    {
        cout << types[i] << endl;
    }
}

