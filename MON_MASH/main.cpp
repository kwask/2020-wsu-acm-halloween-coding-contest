#include <iostream>
#include <string>
#include <sstream>
#include <iterator>
#include <vector>
using namespace std;

/* input:
 * first line N is number of monsters
 * second line is a list of space-separated characters
 * W: werewolf, Z: zombie, H: human
 * 
 * output:
 * resulting line of monsters, separated by spaces 
 */

int main() {
   string input;
   getline(cin, input);
   int num_monsters = stoi(input);

   getline(cin, input);

   // converting space-separated string into a vec of characters
   vector<char> monsters;
   for(uint i = 0; i < input.length(); i++)
   {
       char c = input[i];
       if(c != ' ' && c != '\n' && c != 0)
       {
           monsters.push_back(c); 
       }
   }

   for(uint i = 0; i < monsters.size(); i++)
   {
        if(i > 0 && monsters[i] == 'H') // if human
        {
            if(monsters[i-1] == 'W') // if theres a werewolf ahead
            {
                monsters[i] = 'W'; // werewolf bites the human
            }
            if(monsters[i-1] == 'Z') // if theres a zombie ahead
            {
                monsters[i] = 'Z'; // zombie bites the human
            }
        }
        if(i > 1 && monsters[i] == 'W') // if this was a werewolf
        {
            if(monsters[i-1] == 'W' && monsters[i-2] == 'W') // with 2 werewolves ahead
            {
                // then they all fight and only one survives
                monsters.erase(monsters.begin()+i-2, monsters.begin()+i);
                i -= 2;
            }
        }
   }

   // show results
   string output = "";
   for(uint i = 0; i < monsters.size(); i++)
   {
       if(output.length() > 0)
       {
            output += ' ';
       }
       output += monsters[i]; 
   }
   cout << output;
}
