#include <iostream>
#include <string>
#include <sstream>
#include <cmath>
using namespace std;

double distance(int x0, int y0, int x1, int y1)
{
    return sqrt(pow(abs(x0-x1), 2) + pow(abs(y0-y1), 2)); 
}

int collides(double** p, int psize, int pn)
{
    for(int i = 0; i < psize; i++)
    {
        if(i != pn)
        {
           double d = distance(p[i][0], p[i][1], p[pn][0], p[pn][1]); 
           if(d <= p[i][2] + p[pn][2])
           {
               return i;
           }
        }
    }

    return -1;
}

int main() {  
    string input;
    getline(cin, input);
    int num_pumpkins = stoi(input);
    double* pumpkins[num_pumpkins];
    bool growing[num_pumpkins];

    for(int i = 0; i < num_pumpkins; i++)
    { 
        getline(cin, input);
        stringstream ins;
        ins.str(input);
        pumpkins[i] = new double[3];
        
        ins >> pumpkins[i][0];
        ins >> pumpkins[i][1];
        ins >> pumpkins[i][2]; 
        growing[i] = true;
    }

    bool keep_growing = true;
    double dg = 0.01f;
    while(keep_growing)
    {
        int growers = 0;
        for(int i = 0; i < num_pumpkins; i++)
        {
            if(growing[i])
            {
                pumpkins[i][2] += dg; 
                // check if pumpkin collides with another after growing
                int collider = collides(pumpkins, num_pumpkins, i);
                if(collider != -1)
                {
                    // if it did collide, both it and the collider stop growing
                    growing[collider] = false;
                    growing[i] = false;
                    pumpkins[i][2] -= dg; // undo growth that made them clip 
                }
                else // if there were no collisions after growing
                {
                    growers++; 
                }
            }
        } 

        if(growers < 1)
        {
            keep_growing = false;
        }
    }

    // finding and displaying the largest pumpkin diameter
    double largest = 0.f;
    for(int i = 0; i < num_pumpkins; i++)
    {
        if(pumpkins[i][2] > largest)
        {
            largest = pumpkins[i][2];
        }
    }

    printf("%.2f\n", largest*2);
}
