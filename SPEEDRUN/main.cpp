#include <iostream>
#include <sstream>
using namespace std;

// recursive path search
int predict(int** nh, int xlen, int ylen, int x, int y)
{
    if(x >= xlen || y >= ylen) // if given coords are out of bounds
    {
        return 0;
    } else if(x == xlen-1 && y == ylen-1) // if we're at the bottom right
    {
        return nh[y][x]; 
    }

    int current = nh[y][x]; // the candy from where we are now
    // next two lines find the highest value path from turning either left or
    // right, then we just pick between the two
    int left = predict(nh, xlen, ylen, x+1, y);
    int right = predict(nh, xlen, ylen, x, y+1);
    if(left > right) return current+left;
    else             return current+right;
}

int main() {
    string input;
    getline(cin, input);
    int test_num = stoi(input);
    for(int t = 0; t < test_num; t++)
    {
        // reading grid dimensions
        getline(cin, input);
        stringstream sin;
        sin.str(input);

        int xlen, ylen;
        sin >> xlen;
        sin >> ylen;

        // reading the grid values
        int* nh[ylen];
        for(int y = 0; y < ylen; y++)
        {
            nh[y] = new int[xlen];
            input = "";
            getline(cin, input);
            stringstream lin;
            lin.str(input);
            for(int x = 0; x < xlen; x++)
            {
                string value;
                lin >> value;
                nh[y][x] = stoi(value); 
            }
        }
        
        // find and display the route with highest value
        cout << predict(nh, xlen, ylen, 0, 0) << endl;
    }
}
