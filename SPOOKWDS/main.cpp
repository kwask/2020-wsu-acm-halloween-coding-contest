#include <iostream>
#include <ctype.h>
using namespace std;

// initializes an alphabet array to 0
void init_ab(int* ab)
{
    for(int a = 0; a < 26; a++)
    {
        ab[a] = 0;
    }
}

int main()
{
    string input;
    getline(cin, input);
    int test_num = stoi(input);
    bool matches[test_num];

    for(int i = 0; i < test_num; i++)
    {
        // getting candy types
        getline(cin, input);
        int candy_types = stoi(input);
        int candy_ab[26];
        init_ab(candy_ab);

        // this sums up all occurences of each letter in all candy types
        for(int t = 0; t < candy_types; t++)
        {
            string candy;
            getline(cin, candy);
            for(uint s = 0; s < candy.length(); s++)
            {
                char c = tolower(candy[s]);
                if(c >= 'a' && c <= 'z')
                {
                    candy_ab[c-'a']++;
                }
            }
        }

        getline(cin, input); // throw away equals sign

        string phrase;
        getline(cin, phrase);
        int phrase_ab[26];
        init_ab(phrase_ab);

        // sums up occurences of letters in the given phrase
        for(uint s = 0; s < phrase.length(); s++)
        {
            char c = tolower(phrase[s]);
            if(c >= 'a' && c <= 'z')
            {
                phrase_ab[c-'a']++;
            }
        }

        // finding if the occurences of each letter in the phrase exceed the
        // letters we have from the candy types
        matches[i] = true;
        for(int l = 0; l < 26; l++)
        {
            if(phrase_ab[l] > candy_ab[l])
            {
                matches[i] = false;
            }
        }
    }

    // display results of each test
    for(int i = 0; i < test_num; i++)
    {
        if(matches[i]) cout << "True" << endl;
        else           cout << "False" << endl;
    }
}

